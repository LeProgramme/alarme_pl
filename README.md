# alarme_PL

Ce projet contient les scripts permettant de déclencher une alarme lorsque quelqu'un s'approche de l'ordinateur et entre dans le champ de vision de la webcam.

## detecteur.py
Le script python qui surveille la webcam et lance une des insultes du capitaine haddock si il y a quelqu'un qui vient devant la webcam.
Il doit être lancé depuis un dossier contenant les fichiers mp3 (haddock0.mp3, ..., haddock4.mp3).
Les librairies python necessaires sont : imageio, numpy et visvis.

## Installation
Vous aurez besoin d'installer Python ainsi que certaines librairies. 
Je vous laisse chercher comment faire ça sur l'internet.
J'ai confiance en vous. 