import imageio
import visvis as vv
import numpy as np
import subprocess

#Open the webcam and take two pictures
webcam = imageio.get_reader('<video0>')
last_frame = webcam.get_next_data()
frame = webcam.get_next_data()

##Definition of functions

#Calculate the pixelwise difference between two images avoiding uint8 problems
def difference_image(img1, img2):
    a = img1-img2
    b = np.uint8(img1<img2) * 254 + 1
    return a * b

#Detect movement
th_noise = 10 #The minimal amount of change in a pixel to consider it as realy changing (and not noise)
th_movement = frame.size/2 #the amount of changed pixel necessary to trigger the detector
def movement_detector(frame, last_frame):
    diff = difference_image(frame, last_frame)
    nb_pixels_moved = (diff > th_noise).sum()
    if nb_pixels_moved > th_movement:
        return True
    else:
        return False
#Define an alarm class that play the captain insults without subprocess
class alarm():
    def __init__(self):
        self.sound_process = subprocess.Popen(['true']) #no-op command
        self.sound_number = 0

    def trigger(self):
        if self.sound_process.poll() == None: #Is the process running
            pass
        else : #If not, start a new sound playing
            self.sound_process = subprocess.Popen(['play', 'haddock{0}.mp3'.format(self.sound_number)])
            self.sound_number = (self.sound_number + 1)%5

#Function called every 200 ms. Update the frame and test if there was movement.
alarm = alarm()
def update_frame(event):
    global frame, last_frame
    frame = webcam.get_next_data()
    image.SetData(frame)
    if movement_detector(frame, last_frame):
        alarm.trigger()
    last_frame = frame

a = vv.cla()
a.axis.visible = 0
image = vv.imshow(frame, clim=(0, 255))
timer = vv.Timer(a, interval=200, oneshot=False)
timer.Bind(update_frame)
timer.Start()
app = vv.use()
app.Run()
webcam.close()
